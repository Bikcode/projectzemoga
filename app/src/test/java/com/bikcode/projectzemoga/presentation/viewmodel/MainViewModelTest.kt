package com.bikcode.projectzemoga.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.bikcode.projectzemoga.domain.usecase.DeleteIndividualPostUC
import com.bikcode.projectzemoga.domain.usecase.DeletePostsUC
import com.bikcode.projectzemoga.domain.usecase.PostListUC
import com.bikcode.projectzemoga.domain.usecase.SavePostUC
import com.bikcode.projectzemoga.domain.usecase.UpdatePostUC
import com.bikcode.projectzemoga.presentation.model.Post
import com.bikcode.projectzemoga.presentation.state.PostState
import com.bikcode.projectzemoga.util.CoroutinesTestRule
import com.bikcode.projectzemoga.util.postDAO
import com.bikcode.projectzemoga.util.postPresentation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MainViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    var ruleCoroutine: CoroutinesTestRule = CoroutinesTestRule()

    @Mock
    private lateinit var mockSavePostUC: SavePostUC

    @Mock
    private lateinit var mockPostListUC: PostListUC

    @Mock
    private lateinit var mockUpdatePostUC: UpdatePostUC

    @Mock
    private lateinit var mockDeletePostsUC: DeletePostsUC

    @Mock
    private lateinit var mockDeleteIndividualPostUC: DeleteIndividualPostUC

    @Mock
    private lateinit var mockLivedataPosts: MutableLiveData<PostState>

    @Mock
    private lateinit var mockObserver: Observer<List<Post>>

    @Captor
    private lateinit var captor: ArgumentCaptor<List<Post>>

    private lateinit var mainViewModel: MainViewModel

    private val dispatcher = Dispatchers.Unconfined

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        mainViewModel = MainViewModel(
            mockSavePostUC,
            mockPostListUC,
            mockUpdatePostUC,
            mockDeletePostsUC,
            mockDeleteIndividualPostUC,
            dispatcher,
            mockLivedataPosts
        )
    }

    @After
    fun tearDown() {
        Mockito.verifyNoMoreInteractions(
            mockSavePostUC,
            mockUpdatePostUC,
            mockDeletePostsUC,
            mockDeleteIndividualPostUC,
            mockLivedataPosts
        )
    }


    @Test
    fun fetchAndSavePosts() {
        /** execution */
        mainViewModel.fetchAndSavePosts()

        /** verification */
        runBlocking {
            Mockito.verify(mockSavePostUC).invoke()
        }
    }

    @Test
    fun update() {
        /** execution */
        mainViewModel.update(postPresentation)

        /** verification */
        runBlocking {
            Mockito.verify(mockUpdatePostUC).invoke(postDAO)
            Mockito.verify(mockLivedataPosts).value = PostState.UpdatedPost(postPresentation)
        }
    }

    @Test
    fun deletePosts() {
        /** execution */
        mainViewModel.deletePosts()

        /** verification */
        runBlocking {
            Mockito.verify(mockDeletePostsUC).invoke()
            Mockito.verify(mockLivedataPosts).value = PostState.EmptyList
        }
    }

    @Test
    fun liveData() {
        Assert.assertEquals(
            mockLivedataPosts,
            mainViewModel.postState
        )
    }
}