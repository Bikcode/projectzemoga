package com.bikcode.projectzemoga.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import com.bikcode.projectzemoga.domain.model.CommentDAO
import com.bikcode.projectzemoga.domain.model.UserDAO
import com.bikcode.projectzemoga.domain.usecase.CommentUC
import com.bikcode.projectzemoga.domain.usecase.UserUC
import com.bikcode.projectzemoga.presentation.model.Comment
import com.bikcode.projectzemoga.presentation.model.User
import com.bikcode.projectzemoga.presentation.state.DetailState
import com.bikcode.projectzemoga.util.mock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class PostDetailViewModelTest {

    @Mock
    private lateinit var mockUserUC: UserUC

    @Mock
    private lateinit var mockCommentUC: CommentUC

    @Mock
    private lateinit var mockDetailStateLivedata: MutableLiveData<DetailState>

    private val dispatcher = Dispatchers.Unconfined

    private lateinit var postDetailViewModel: PostDetailViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        postDetailViewModel = PostDetailViewModel(
            mockUserUC,
            mockCommentUC,
            dispatcher,
            mockDetailStateLivedata
        )
    }

    @After
    fun tearDown() {
        Mockito.verifyNoMoreInteractions(
            mockUserUC,
            mockCommentUC,
            mockDetailStateLivedata
        )
    }

    @Test
    fun getDetailState() {
        assertEquals(
            mockDetailStateLivedata,
            postDetailViewModel.detailState
        )
    }

    @Test
    fun `getUser should return a user`(): Unit = runBlocking {
        /** preparation */
        val mockUserDao: UserDAO = mock()
        val mockUserPresentation: User = mock()
        Mockito.`when`(mockUserDao.userToPresentation()).thenReturn(mockUserPresentation)
        Mockito.`when`(mockUserUC.invoke(1)).thenReturn(mockUserDao)

        /** execution */
        postDetailViewModel.getUser(1)

        /** verification */
        Mockito.verify(mockUserUC).invoke(1)
        Mockito.verify(mockUserDao).userToPresentation()
        Mockito.verify(mockDetailStateLivedata).value =
            DetailState.OnSuccessUser(mockUserPresentation)
        Mockito.verifyNoMoreInteractions(
            mockUserDao,
            mockUserPresentation
        )
    }

    @Test
    fun `getUser should return a null user`(): Unit = runBlocking {
        /** preparation */
        Mockito.`when`(mockUserUC.invoke(1)).thenReturn(null)

        /** execution */
        postDetailViewModel.getUser(1)

        /** verification */
        Mockito.verify(mockUserUC).invoke(1)
        Mockito.verify(mockDetailStateLivedata).value = DetailState.UserNull
    }

    @Test
    fun `getUser should return exception`(): Unit = runBlocking {
        /** preparation */
        Mockito.`when`(mockUserUC.invoke(1)).thenThrow(
            IllegalArgumentException("Error")
        )

        /** execution */
        postDetailViewModel.getUser(1)

        /** verification */
        Mockito.verify(mockUserUC).invoke(1)
        Mockito.verify(mockDetailStateLivedata).value = DetailState.OnErrorUser("Error")
    }

    @Test
    fun `getComments should return a success list of comments`() {
        /** preparation */
        val mockCommentDAO: CommentDAO = mock()
        val mockComment: Comment = mock()
        Mockito.`when`(mockCommentDAO.commentToPresentation()).thenReturn(mockComment)
        Mockito.`when`(mockCommentUC.invoke(1)).thenReturn(flowOf(
            listOf(mockCommentDAO)
        ))

        /** execution */
        postDetailViewModel.getComments(1)

        /** verification */
        Mockito.verify(mockCommentUC).invoke(1)
        Mockito.verify(mockCommentDAO).commentToPresentation()
        Mockito.verify(mockDetailStateLivedata).value =
            DetailState.OnSuccessComments(listOf(mockComment))
        Mockito.verifyNoMoreInteractions(
            mockCommentDAO,
            mockComment
        )
    }

    @Test
    fun `getComments should return null list of comments`() {
        /** preparation */
        Mockito.`when`(mockCommentUC.invoke(1)).thenReturn(flowOf(null))

        /** execution */
        postDetailViewModel.getComments(1)

        /** verification */
        Mockito.verify(mockCommentUC).invoke(1)
        Mockito.verify(mockDetailStateLivedata).value = DetailState.CommentsNull
    }

    @Test
    fun `getComments should return a error`() {
        /** preparation */
        Mockito.`when`(mockCommentUC.invoke(1)).thenThrow(
            java.lang.IllegalArgumentException("Error")
        )

        /** execution */
        postDetailViewModel.getComments(1)

        /** verification */
        Mockito.verify(mockCommentUC).invoke(1)
        Mockito.verify(mockDetailStateLivedata).value = DetailState.OnErrorComments("Error")
    }
}