package com.bikcode.projectzemoga.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import com.bikcode.projectzemoga.domain.model.PostDAO
import com.bikcode.projectzemoga.domain.usecase.FavoritePostsUC
import com.bikcode.projectzemoga.presentation.model.Post
import com.bikcode.projectzemoga.presentation.state.FavoriteState
import com.bikcode.projectzemoga.util.mock
import kotlinx.coroutines.flow.flowOf
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class FavoriteViewModelTest {

    @Mock
    private lateinit var mockFavoritePostUC: FavoritePostsUC

    @Mock
    private lateinit var mockFavoriteStateLivedata: MutableLiveData<FavoriteState>

    private lateinit var favoriteViewModel: FavoriteViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        favoriteViewModel = FavoriteViewModel(
            mockFavoritePostUC,
            mockFavoriteStateLivedata
        )
    }

    @After
    fun tearDown() {
        Mockito.verifyNoMoreInteractions(
            mockFavoritePostUC,
            mockFavoriteStateLivedata
        )
    }

    @Test
    fun `getFavoriteState should return a list of posts`() {
        val mockPostDAO: PostDAO = mock()
        val mockPost: Post = mock()
        /** preparation */
        Mockito.`when`(mockPostDAO.postToPresentation()).thenReturn(mockPost)
        Mockito.`when`(mockFavoritePostUC.invoke()).thenReturn(
            flowOf(listOf(mockPostDAO))
        )

        /** execution */
        favoriteViewModel.getFavoritePosts()

        /** verification */
        Mockito.verify(mockFavoritePostUC).invoke()
        Mockito.verify(mockPostDAO).postToPresentation()
        Mockito.verify(mockFavoriteStateLivedata).value = FavoriteState.OnSuccessFavoriteList(
            listOf(mockPost)
        )
    }

    @Test
    fun `getFavoriteState should return a empty list`() {
        /** preparation */
        Mockito.`when`(mockFavoritePostUC.invoke()).thenReturn(
            flowOf(emptyList())
        )

        /** execution */
        favoriteViewModel.getFavoritePosts()

        /** verification */
        Mockito.verify(mockFavoritePostUC).invoke()
        Mockito.verify(mockFavoriteStateLivedata).value = FavoriteState.EmptyList
    }

    @Test
    fun getFavoritePosts() {
        /** preparation */
        Mockito.`when`(mockFavoritePostUC.invoke()).thenThrow(
            IllegalArgumentException("Error")
        )

        /** execution*/
        favoriteViewModel.getFavoritePosts()

        /** verification */
        Mockito.verify(mockFavoritePostUC).invoke()
        Mockito.verify(mockFavoriteStateLivedata).value = FavoriteState.OnError("Error")
    }
}