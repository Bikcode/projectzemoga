package com.bikcode.projectzemoga.domain.usecase

import com.bikcode.projectzemoga.domain.repository.PostRepository
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class DeletePostsUCTest {

    @Mock
    private lateinit var mockPostRepository: PostRepository

    private lateinit var deletePostsUC: DeletePostsUC

    @Before
    fun setUp() {
         MockitoAnnotations.openMocks(this)
        deletePostsUC = DeletePostsUC(mockPostRepository)
    }

    @After
    fun tearDown() {
        Mockito.verifyNoMoreInteractions(mockPostRepository)
    }

    @Test
    operator fun invoke(): Unit = runBlocking {
        /** execution */
        deletePostsUC.invoke()

        /** verification */
        Mockito.verify(mockPostRepository).deletePosts()
    }
}