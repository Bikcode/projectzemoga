package com.bikcode.projectzemoga.domain.usecase

import com.bikcode.projectzemoga.domain.model.PostDAO
import com.bikcode.projectzemoga.domain.repository.PostRepository
import com.bikcode.projectzemoga.util.mock
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class FavoritePostsUCTest {

    @Mock
    private lateinit var mockPostRepository: PostRepository

    private lateinit var favoritePostsUC: FavoritePostsUC

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        favoritePostsUC = FavoritePostsUC(mockPostRepository)
    }

    @After
    fun tearDown() {
        Mockito.verifyNoMoreInteractions(mockPostRepository)
    }

    @Test
    fun `invoke should`(): Unit = runBlocking {
        /** preparation */
        val mockPostDAO: PostDAO = mock()
        Mockito.`when`(mockPostRepository.getFavoritePosts()).thenReturn(
            flowOf(listOf(mockPostDAO))
        )
        /** execution */
        val result = favoritePostsUC.invoke()

        /** verification */
        result.collect {
            assertEquals(
                listOf(mockPostDAO),
                it
            )
        }
        Mockito.verify(mockPostRepository).getFavoritePosts()
        Mockito.verifyNoMoreInteractions(mockPostDAO)
    }

    @Test
    fun `invoke should return a empty list`(): Unit = runBlocking {
        /** preparation */

        Mockito.`when`(mockPostRepository.getFavoritePosts()).thenReturn(
            flowOf(listOf())
        )
        /** execution */
        val result = favoritePostsUC.invoke()

        /** verification */
        result.collect {
            assertEquals(
                listOf<PostDAO>(),
                it
            )
        }
        Mockito.verify(mockPostRepository).getFavoritePosts()
    }
}