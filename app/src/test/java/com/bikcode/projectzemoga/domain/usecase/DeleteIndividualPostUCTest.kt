package com.bikcode.projectzemoga.domain.usecase

import com.bikcode.projectzemoga.domain.model.PostDAO
import com.bikcode.projectzemoga.domain.repository.PostRepository
import com.bikcode.projectzemoga.util.mock
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class DeleteIndividualPostUCTest {

    @Mock
    private lateinit var mockPostRepository: PostRepository

    @Mock
    private lateinit var mockPostDAO: PostDAO

    private lateinit var deleteIndividualPostUC: DeleteIndividualPostUC

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        deleteIndividualPostUC = DeleteIndividualPostUC(mockPostRepository)
    }

    @After
    fun tearDown() {
        Mockito.verifyNoMoreInteractions(mockPostRepository, mockPostDAO)
    }

    @Test
    fun `invoke should return 1 for deleted post`(): Unit = runBlocking {
        /** preparation */
        Mockito.`when`(mockPostRepository.deleteIndividualPost(mockPostDAO)).thenReturn(1)

        /** execution */
        val result = deleteIndividualPostUC.invoke(mockPostDAO)
        /** verification */
        assertEquals(
            1,
            result
        )
        Mockito.verify(mockPostRepository).deleteIndividualPost(mockPostDAO)
    }

    @Test
    fun `invoke should return 0 when no delete the post`(): Unit = runBlocking {
        /** preparation */
        val mockPostDAO: PostDAO = mock()
        Mockito.`when`(mockPostRepository.deleteIndividualPost(mockPostDAO)).thenReturn(0
        )
        /** execution */
        val result = deleteIndividualPostUC.invoke(mockPostDAO)

        /** verification */
        assertEquals(
            0,
            result
        )
        Mockito.verify(mockPostRepository).deleteIndividualPost(mockPostDAO)
    }
}