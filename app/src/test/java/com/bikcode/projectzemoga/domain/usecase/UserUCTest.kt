package com.bikcode.projectzemoga.domain.usecase

import com.bikcode.projectzemoga.domain.model.UserDAO
import com.bikcode.projectzemoga.domain.repository.UserRepository
import com.bikcode.projectzemoga.util.mock
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class UserUCTest {

    @Mock
    private lateinit var mockUserRepository: UserRepository

    private lateinit var userUC: UserUC

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        userUC = UserUC(mockUserRepository)
    }

    @After
    fun tearDown() {
        Mockito.verifyNoMoreInteractions(mockUserRepository)
    }

    @Test
    fun `invoke should return a user`(): Unit = runBlocking {
        /** preparation */
        val mockUserDAO: UserDAO = mock()
        Mockito.`when`(mockUserRepository.getUserFromPost(1)).thenReturn(
            mockUserDAO
        )
        /** execution */
        val result = userUC.invoke(1)

        /** verification */
        Assert.assertEquals(
            mockUserDAO,
            result
        )
        Mockito.verify(mockUserRepository).getUserFromPost(1)
    }

    @Test
    fun `invoke should return null`(): Unit = runBlocking {
        /** preparation */
        Mockito.`when`(mockUserRepository.getUserFromPost(1)).thenReturn(
            null
        )
        /** execution */
        val result = userUC.invoke(1)

        /** verification */
        Assert.assertEquals(
            null,
            result
        )
        Mockito.verify(mockUserRepository).getUserFromPost(1)
    }

    @Test
    fun `invoke should return exception`(): Unit = runBlocking {
        /** preparation */
        Mockito.`when`(mockUserRepository.getUserFromPost(1)).thenThrow(
            IllegalArgumentException("Error")
        )
        /** execution */
        try {
            userUC.invoke(1)
        } catch (exception: Exception) {
            /** verification */
            assertTrue(
                exception is IllegalArgumentException
            )
        }

        /** verification */
        Mockito.verify(mockUserRepository).getUserFromPost(1)
    }
}