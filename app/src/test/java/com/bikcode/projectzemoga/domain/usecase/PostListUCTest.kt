package com.bikcode.projectzemoga.domain.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.bikcode.projectzemoga.domain.model.PostDAO
import com.bikcode.projectzemoga.domain.repository.PostRepository
import com.bikcode.projectzemoga.util.mock
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class PostListUCTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var mockPostRepository: PostRepository

    private lateinit var postListUC: PostListUC

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        postListUC = PostListUC(mockPostRepository)
    }

    @After
    fun tearDown() {
        Mockito.verifyNoMoreInteractions(mockPostRepository)
    }

    @Test
    fun `invoke should return a list`() = runBlocking {
        /** preparation */
        val mockPostDAO: PostDAO = mock()
        val flow = flow { emit(listOf(mockPostDAO)) }
        Mockito.`when`(mockPostRepository.getPostsLocal()).thenReturn(
            flow
        )
        /** execution */

        /** verification */
        val result = postListUC.invoke()

        result.collect {
            assertEquals(
                listOf(mockPostDAO),
                it
            )
        }
        Mockito.verify(mockPostRepository).getPostsLocal()
        Mockito.verifyNoMoreInteractions(mockPostDAO)
    }

    @Test
    fun `invoke should return a empty list`() {
        /** preparation */
        val flow = flow { emit(listOf<PostDAO>()) }
        Mockito.`when`(mockPostRepository.getPostsLocal()).thenReturn(
            flow
        )
        /** execution */

        /** verification */
        runBlocking {
            val result = postListUC.invoke()

            result.collect {
                assertEquals(
                    emptyList<PostDAO>(),
                    it
                )
            }
            Mockito.verify(mockPostRepository).getPostsLocal()
        }
    }
}