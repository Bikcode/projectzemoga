package com.bikcode.projectzemoga.domain.usecase

import com.bikcode.projectzemoga.domain.model.PostDAO
import com.bikcode.projectzemoga.domain.repository.PostRepository
import com.bikcode.projectzemoga.util.mock
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class UpdatePostUCTest {

    @Mock
    private lateinit var mockPostRepository: PostRepository

    private lateinit var updatePostUC: UpdatePostUC

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        updatePostUC = UpdatePostUC(mockPostRepository)
    }

    @After
    fun tearDown() {
        Mockito.verifyNoMoreInteractions(mockPostRepository)
    }

    @Test
    fun invoke(): Unit = runBlocking {
        /** preparation */
        val mockPostDAO: PostDAO = mock()

        /** execution */
        updatePostUC.invoke(mockPostDAO)

        /** verification */
        Mockito.verify(mockPostRepository).updatePost(mockPostDAO)
    }
}