package com.bikcode.projectzemoga.domain.usecase

import com.bikcode.projectzemoga.domain.repository.PostRepository
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class SavePostUCTest {

    @Mock
    private lateinit var mockPostRepository: PostRepository

    private lateinit var savePostUC: SavePostUC

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        savePostUC = SavePostUC(mockPostRepository)
    }

    @After
    fun tearDown() {
        Mockito.verifyNoMoreInteractions(mockPostRepository)
    }

    @Test
    fun invoke(): Unit = runBlocking {
        /** execution */
        savePostUC.invoke()

        /** verification */
        Mockito.verify(mockPostRepository).savePost()
    }
}