package com.bikcode.projectzemoga.domain.usecase

import com.bikcode.projectzemoga.domain.model.CommentDAO
import com.bikcode.projectzemoga.domain.repository.CommentRepository
import com.bikcode.projectzemoga.util.mock
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class CommentUCTest {

    @Mock
    private lateinit var mockCommentRepository: CommentRepository

    private lateinit var commentUC: CommentUC

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        commentUC = CommentUC(mockCommentRepository)
    }

    @After
    fun tearDown() {
        Mockito.verifyNoMoreInteractions(mockCommentRepository)
    }

    @Test
    fun `invoke should return a list of comments`(): Unit = runBlocking {
        /** preparation */
        val mockCommentDAO: CommentDAO = mock()
        Mockito.`when`(mockCommentRepository.getCommentsFromPost(1)).thenReturn(
            flowOf(listOf(mockCommentDAO))
        )
        /** execution */
        val result = commentUC.invoke(1)

        /** verification */
        result.collect {
            assertEquals(
                listOf(mockCommentDAO),
                it
            )
        }
        Mockito.verify(mockCommentRepository).getCommentsFromPost(1)
        Mockito.verifyNoMoreInteractions(mockCommentDAO)
    }

    @Test
    fun `invoke should return a  null list of comments`(): Unit = runBlocking {
        Mockito.`when`(mockCommentRepository.getCommentsFromPost(1)).thenReturn(
            flowOf(listOf())
        )
        /** execution */
        val result = commentUC.invoke(1)

        /** verification */
        result.collect {
            assertEquals(
                emptyList<CommentDAO>(),
                it
            )
        }
        Mockito.verify(mockCommentRepository).getCommentsFromPost(1)
    }

    @Test
    fun `invoke should catch a exception`(): Unit = runBlocking {
        Mockito.`when`(mockCommentRepository.getCommentsFromPost(1)).thenThrow(
            IllegalArgumentException("Error")
        )
        /** execution */

        try {
            commentUC.invoke(1)
        }catch (exception: Exception) {
            assertTrue(
                exception is IllegalArgumentException
            )
            assertEquals(
                "Error",
                exception.message
            )
        }
        Mockito.verify(mockCommentRepository).getCommentsFromPost(1)
    }
}