package com.bikcode.projectzemoga.data.repository

import com.bikcode.projectzemoga.data.remote.dto.PostDTO
import com.bikcode.projectzemoga.data.remote.services.PostService
import com.bikcode.projectzemoga.domain.datasource.LocalDataSource
import com.bikcode.projectzemoga.domain.repository.PostRepository
import com.bikcode.projectzemoga.util.mock
import com.bikcode.projectzemoga.util.postDAO
import com.bikcode.projectzemoga.util.postDTO
import com.bikcode.projectzemoga.util.postEntity
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.BDDMockito.`when`
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.MockitoAnnotations
import retrofit2.Response

class PostRepositoryImplTest {

    @Mock
    private lateinit var mockPostService: PostService

    private lateinit var postRepository: PostRepository

    @Mock
    private lateinit var mockLocalDataSource: LocalDataSource

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        postRepository = PostRepositoryImpl(
            mockPostService,
            1,
            mockLocalDataSource
        )
    }

    @After
    fun tearDown() {
        verifyNoMoreInteractions(mockPostService, mockLocalDataSource)
    }

    @Test
    fun savePost(): Unit = runBlocking {
        /** preparation */
        val mockResponse = mock<Response<List<PostDTO>>>()

        `when`(mockLocalDataSource.getCount()).thenReturn(0)

        `when`(mockResponse.body()).thenReturn(
            listOf(postDTO)
        )
        `when`(mockPostService.getAllPosts()).thenReturn(
            mockResponse
        )
        /** execution */
        postRepository.savePost()

        /** verification */
        Mockito.verify(mockLocalDataSource).savePost(postEntity)
        Mockito.verify(mockPostService).getAllPosts()
        Mockito.verify(mockResponse).body()
        Mockito.verify(mockLocalDataSource).getCount()
    }

    @Test
    fun `getAllPosts should return a exception`(): Unit = runBlocking {
        /** preparation */
        `when`(mockLocalDataSource.getCount()).thenReturn(0)
        `when`(mockPostService.getAllPosts()).thenThrow(
            java.lang.IllegalArgumentException("Error")
        )

        /** execution */
        try {
            postRepository.savePost()
        } catch (ex: Exception) {
            /** verification */
            Assert.assertTrue(ex is IllegalArgumentException)
            Mockito.verify(mockPostService).getAllPosts()
            Mockito.verify(mockLocalDataSource).getCount()
        }
    }

    @Test
    fun getPostsLocal() {
        /** preparation */
        `when`(mockLocalDataSource.getPosts()).thenReturn(flowOf(listOf(postDAO)))

        /** execution */
        val result = postRepository.getPostsLocal()

        /** verification */
        runBlocking {
            result.collect {
                Assert.assertEquals(
                    listOf(postDAO),
                    it
                )
            }
            Mockito.verify(mockLocalDataSource).getPosts()
        }
    }

    @Test
    fun getFavoritePosts() {
        /** preparation */
        `when`(mockLocalDataSource.getFavoritePosts()).thenReturn(flowOf(listOf(postDAO)))

        /** execution */
        val result = postRepository.getFavoritePosts()

        /** verification */
        runBlocking {
            result.collect {
                Assert.assertEquals(
                    listOf(postDAO),
                    it
                )
            }
            Mockito.verify(mockLocalDataSource).getFavoritePosts()
        }
    }

    @Test
    fun updatePosts() {
        runBlocking {
            /** execution */
            postRepository.updatePost(postDAO)

            /** verification */
            Mockito.verify(mockLocalDataSource).updatePost(postDAO)
        }
    }

    @Test
    fun deletePosts() {
        runBlocking {
            /** execution */
            postRepository.deletePosts()

            /** verification */
            Mockito.verify(mockLocalDataSource).deletePosts()
        }
    }
}