package com.bikcode.projectzemoga.data.repository

import com.bikcode.projectzemoga.data.remote.dto.CommentDTO
import com.bikcode.projectzemoga.data.remote.services.CommentService
import com.bikcode.projectzemoga.domain.model.CommentDAO
import com.bikcode.projectzemoga.domain.repository.CommentRepository
import com.bikcode.projectzemoga.util.mock
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

class CommentRepositoryImplTest {

    @Mock
    private lateinit var mockCommentService: CommentService

    private lateinit var commentRepository: CommentRepository

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        commentRepository = CommentRepositoryImpl(mockCommentService)
    }

    @After
    fun tearDown() {
        Mockito.verifyNoMoreInteractions(
            mockCommentService
        )
    }

    @Test
    fun `getCommentsFromPost should return a list of comments`(): Unit = runBlocking {
        /** preparation */
        val mockCommentDTO: CommentDTO = mock()
        val mockResponse: Response<List<CommentDTO>> = mock()
        val mockCommentDAO: CommentDAO = mock()
        Mockito.`when`(mockResponse.isSuccessful).thenReturn(true)
        Mockito.`when`(mockResponse.body()).thenReturn(listOf(mockCommentDTO))
        Mockito.`when`(mockCommentDTO.commentToDAO()).thenReturn(mockCommentDAO)
        Mockito.`when`(mockCommentService.getAllComments("1")).thenReturn(
            mockResponse
        )

        /** execution */
        val result = commentRepository.getCommentsFromPost(1)

        /** verification */
        result.collect {
            Assert.assertEquals(
                listOf(mockCommentDAO),
                it
            )
        }
        Mockito.verify(mockCommentService).getAllComments("1")
        Mockito.verify(mockResponse).isSuccessful
        Mockito.verify(mockResponse).body()
        Mockito.verify(mockCommentDTO).commentToDAO()
        Mockito.verifyNoMoreInteractions(
            mockCommentDAO,
            mockCommentDTO,
            mockResponse
        )
    }

    @Test
    fun `getCommentsFromPost should return null`(): Unit = runBlocking {
        /** preparation */
        val mockResponse: Response<List<CommentDTO>> = mock()
        Mockito.`when`(mockResponse.isSuccessful).thenReturn(false)
        Mockito.`when`(mockCommentService.getAllComments("1")).thenReturn(
            mockResponse
        )

        /** execution */
        val result = commentRepository.getCommentsFromPost(1)

        /** verification */
        result.collect {
            Assert.assertEquals(
                null,
                it
            )
        }
        Mockito.verify(mockCommentService).getAllComments("1")
        Mockito.verify(mockResponse).isSuccessful
        Mockito.verifyNoMoreInteractions(
            mockResponse
        )
    }

    @Test
    fun `getCommentsFromPost should return a exception`(): Unit = runBlocking {
        /** preparation */
        Mockito.`when`(mockCommentService.getAllComments("1")).thenThrow(
            IllegalArgumentException("Error")
        )

        /** execution */
        val result = commentRepository.getCommentsFromPost(1)

        /** verification */
        result.catch {
            Assert.assertTrue(
                it is IllegalArgumentException
            )
        }
    }
}