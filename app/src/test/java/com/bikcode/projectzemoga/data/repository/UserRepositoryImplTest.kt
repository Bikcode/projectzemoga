package com.bikcode.projectzemoga.data.repository

import com.bikcode.projectzemoga.data.remote.dto.UserDTO
import com.bikcode.projectzemoga.data.remote.services.UserService
import com.bikcode.projectzemoga.domain.model.UserDAO
import com.bikcode.projectzemoga.domain.repository.UserRepository
import com.bikcode.projectzemoga.util.mock
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

class UserRepositoryImplTest {

    @Mock
    private lateinit var mockUserService: UserService

    private lateinit var userRepository: UserRepository

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        userRepository = UserRepositoryImpl(mockUserService)
    }

    @After
    fun tearDown() {
        Mockito.verifyNoMoreInteractions(mockUserService)
    }

    @Test
    fun `getUserFromPost should return a user`(): Unit = runBlocking {
        /** preparation */
        val mockUserDTO: UserDTO = mock()
        val mockUserDAO: UserDAO = mock()
        val mockResponse: Response<UserDTO> = mock()
        Mockito.`when`(mockResponse.isSuccessful).thenReturn(true)
        Mockito.`when`(mockResponse.body()).thenReturn(mockUserDTO)
        Mockito.`when`(mockUserDTO.userToDAO()).thenReturn(mockUserDAO)
        Mockito.`when`(mockUserService.getUser("1")).thenReturn(
            mockResponse
        )

        /** execution */
        val result = userRepository.getUserFromPost(1)

        /** verification */
        Assert.assertEquals(
            mockUserDAO,
            result
        )
        Mockito.verify(mockUserService).getUser("1")
        Mockito.verify(mockUserDTO).userToDAO()
        Mockito.verify(mockResponse).isSuccessful
        Mockito.verify(mockResponse).body()
        Mockito.verifyNoMoreInteractions(
            mockUserDAO,
            mockUserDTO,
            mockResponse
        )
    }

    @Test
    fun `getUserFromPost should return null`(): Unit = runBlocking {
        /** preparation */
        val mockResponse: Response<UserDTO> = mock()
        Mockito.`when`(mockResponse.isSuccessful).thenReturn(false)
        Mockito.`when`(mockUserService.getUser("1")).thenReturn(
            mockResponse
        )

        /** execution */
        val result = userRepository.getUserFromPost(1)

        /** verification */
        Assert.assertEquals(
            null,
            result
        )
        Mockito.verify(mockUserService).getUser("1")
        Mockito.verify(mockResponse).isSuccessful
        Mockito.verifyNoMoreInteractions(mockResponse)
    }

    @Test
    fun `getUserFromPost should return a exception`(): Unit = runBlocking {
        /** preparation */
        Mockito.`when`(mockUserService.getUser("1")).thenThrow(
            IllegalArgumentException("Error")
        )

        /** execution */
        try {
            userRepository.getUserFromPost(1)
        }catch (exception: Exception) {
            /** verification */
            Assert.assertTrue(
                exception is IllegalArgumentException
            )
        }

        /** verification */
        Mockito.verify(mockUserService).getUser("1")
    }
}