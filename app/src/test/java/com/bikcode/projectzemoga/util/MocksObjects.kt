package com.bikcode.projectzemoga.util

import com.bikcode.projectzemoga.data.remote.dto.PostDTO
import com.bikcode.projectzemoga.domain.entity.PostEntity
import com.bikcode.projectzemoga.domain.model.PostDAO
import com.bikcode.projectzemoga.presentation.model.Post

val postPresentation = Post(
    "body",
    1,
    "title",
    1,
    isVisited = false,
    isFavorite = false
)

val postDAO = PostDAO(
    1,
    "body",
    "title",
    1,
    isVisited = false,
    isFavorite = false
)

val postDTO = PostDTO(
    "body",
    1,
    "title",
    1,
)

val postEntity = PostEntity(
    1,
    "body",
    "title",
    1,
    isVisited = false,
    isFavorite = false
)