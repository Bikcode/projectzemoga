package com.bikcode.projectzemoga.di

import androidx.lifecycle.MutableLiveData
import com.bikcode.projectzemoga.presentation.state.DetailState
import com.bikcode.projectzemoga.presentation.state.FavoriteState
import com.bikcode.projectzemoga.presentation.state.PostState
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object LiveDataModule {

    @Provides
    fun liveDataPostsProvider(): MutableLiveData<PostState> = MutableLiveData()

    @Provides
    fun liveDataFavoritePostsProvider(): MutableLiveData<FavoriteState> = MutableLiveData()

    @Provides
    fun livedataDetailPostProvider(): MutableLiveData<DetailState> = MutableLiveData()
}