package com.bikcode.projectzemoga.di.repository

import com.bikcode.projectzemoga.data.local.AppDatabase
import com.bikcode.projectzemoga.data.local.dao.PostDao
import com.bikcode.projectzemoga.data.remote.services.CommentService
import com.bikcode.projectzemoga.data.remote.services.PostService
import com.bikcode.projectzemoga.data.remote.services.UserService
import com.bikcode.projectzemoga.data.repository.CommentRepositoryImpl
import com.bikcode.projectzemoga.data.repository.PostRepositoryImpl
import com.bikcode.projectzemoga.data.repository.UserRepositoryImpl
import com.bikcode.projectzemoga.domain.datasource.LocalDataSource
import com.bikcode.projectzemoga.domain.repository.CommentRepository
import com.bikcode.projectzemoga.domain.repository.PostRepository
import com.bikcode.projectzemoga.domain.repository.UserRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object RepositoryModule {

    @Provides
    fun postRepositoryBind(
        postService: PostService,
        postUnread: Int,
        localDataSource: LocalDataSource,
    ): PostRepository =
        PostRepositoryImpl(postService, postUnread, localDataSource)

    @Provides
    fun postUnreadProvider(): Int = 20

    @Provides
    fun postDaoProvider(appDatabase: AppDatabase): PostDao {
        return appDatabase.postDao()
    }

    @Provides
    fun userRepositoryProvider(userService: UserService): UserRepository =
        UserRepositoryImpl(userService)

    @Provides
    fun commentRepositoryProvider(commentService: CommentService): CommentRepository =
        CommentRepositoryImpl(commentService)
}