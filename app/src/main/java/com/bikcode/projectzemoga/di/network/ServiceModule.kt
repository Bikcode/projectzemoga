package com.bikcode.projectzemoga.di.network

import com.bikcode.projectzemoga.data.remote.services.CommentService
import com.bikcode.projectzemoga.data.remote.services.PostService
import com.bikcode.projectzemoga.data.remote.services.UserService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    @Singleton
    @Provides
    fun commentServiceProvider(retrofit: Retrofit): CommentService =
        retrofit.create(CommentService::class.java)

    @Singleton
    @Provides
    fun postServiceProvider(retrofit: Retrofit): PostService =
        retrofit.create(PostService::class.java)

    @Singleton
    @Provides
    fun userServiceProvider(retrofit: Retrofit): UserService =
        retrofit.create(UserService::class.java)
}