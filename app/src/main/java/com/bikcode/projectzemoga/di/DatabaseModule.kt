package com.bikcode.projectzemoga.di

import android.content.Context
import androidx.room.Room
import com.bikcode.projectzemoga.data.local.AppDatabase
import com.bikcode.projectzemoga.data.local.RoomDataSource
import com.bikcode.projectzemoga.data.local.dao.PostDao
import com.bikcode.projectzemoga.domain.datasource.LocalDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun appDatabaseProvider(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            AppDatabase.DATABASE_NAME
        ).build()
    }

    @Provides
    fun postDaoProvider(appDatabase: AppDatabase): PostDao =
        appDatabase.postDao()

    @Provides
    fun roomDataSourceProvider(postDao: PostDao): LocalDataSource =
        RoomDataSource(postDao)
}