package com.bikcode.projectzemoga.di.usecase

import com.bikcode.projectzemoga.domain.repository.CommentRepository
import com.bikcode.projectzemoga.domain.repository.PostRepository
import com.bikcode.projectzemoga.domain.repository.UserRepository
import com.bikcode.projectzemoga.domain.usecase.CommentUC
import com.bikcode.projectzemoga.domain.usecase.DeleteIndividualPostUC
import com.bikcode.projectzemoga.domain.usecase.DeletePostsUC
import com.bikcode.projectzemoga.domain.usecase.FavoritePostsUC
import com.bikcode.projectzemoga.domain.usecase.PostListUC
import com.bikcode.projectzemoga.domain.usecase.SavePostUC
import com.bikcode.projectzemoga.domain.usecase.UpdatePostUC
import com.bikcode.projectzemoga.domain.usecase.UserUC
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object UseCaseModule {

    @Provides
    fun savePostUseCaseProvider(postRepository: PostRepository): SavePostUC {
        return SavePostUC(postRepository)
    }

    @Provides
    fun getPostsUseCaseProvider(postRepository: PostRepository): PostListUC {
        return PostListUC(postRepository)
    }

    @Provides
    fun updatePostsUseCaseProvider(postRepository: PostRepository): UpdatePostUC {
        return UpdatePostUC(postRepository)
    }

    @Provides
    fun deletePostsUseCaseProvider(postRepository: PostRepository): DeletePostsUC {
        return DeletePostsUC(postRepository)
    }

    @Provides
    fun favoritePostsUseCaseProvider(postRepository: PostRepository): FavoritePostsUC {
        return FavoritePostsUC(postRepository)
    }

    @Provides
    fun userUseCaseProvider(userRepository: UserRepository): UserUC {
        return UserUC(userRepository)
    }

    @Provides
    fun commentUseCaseProvider(commentRepository: CommentRepository): CommentUC {
        return CommentUC(commentRepository)
    }

    @Provides
    fun deleteIndividualPostUC(postRepository: PostRepository): DeleteIndividualPostUC =
        DeleteIndividualPostUC(postRepository)
}