package com.bikcode.projectzemoga.domain.usecase

import com.bikcode.projectzemoga.domain.model.UserDAO
import com.bikcode.projectzemoga.domain.repository.UserRepository

class UserUC(private val userRepository: UserRepository) {
    suspend operator fun invoke(userId: Int): UserDAO? = userRepository.getUserFromPost(userId)
}