package com.bikcode.projectzemoga.domain.datasource

import com.bikcode.projectzemoga.domain.entity.PostEntity
import com.bikcode.projectzemoga.domain.model.PostDAO
import kotlinx.coroutines.flow.Flow

interface LocalDataSource {
    suspend fun savePost(postEntity: PostEntity)
    fun getPosts(): Flow<List<PostDAO>>
    suspend fun getCount(): Int
    suspend fun updatePost(postDAO: PostDAO)
    suspend fun deletePosts()
    fun getFavoritePosts(): Flow<List<PostDAO>>
    suspend fun deletePost(post: PostDAO): Int
}