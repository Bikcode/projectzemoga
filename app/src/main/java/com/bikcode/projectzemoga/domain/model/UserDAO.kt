package com.bikcode.projectzemoga.domain.model

import com.bikcode.projectzemoga.presentation.model.User

data class UserDAO(
    val id: Int,
    val name: String,
    val email: String,
    val phone: String,
    val website: String,
) {
    fun userToPresentation(): User = User(
        id,
        name,
        email,
        phone,
        website
    )
}