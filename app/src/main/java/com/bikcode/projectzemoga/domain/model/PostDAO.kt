package com.bikcode.projectzemoga.domain.model

import com.bikcode.projectzemoga.domain.entity.PostEntity
import com.bikcode.projectzemoga.presentation.model.Post

data class PostDAO(
    val id: Int,
    val body: String,
    val title: String,
    val userId: Int,
    val isVisited: Boolean,
    val isFavorite: Boolean,
) {
    fun postToPresentation(): Post =
        Post(
            body,
            id,
            title,
            userId,
            isVisited,
            isFavorite
        )

    fun postToEntity(): PostEntity = PostEntity(
        id,
        body,
        title,
        userId,
        isVisited,
        isFavorite
    )
}