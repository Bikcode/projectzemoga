package com.bikcode.projectzemoga.domain.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bikcode.projectzemoga.domain.model.PostDAO

@Entity(tableName = "post")
data class PostEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val body: String,
    val title: String,
    val userId: Int,
    val isVisited: Boolean,
    val isFavorite: Boolean
) {
    fun postToDAO(): PostDAO = PostDAO(
        id,
        body,
        title,
        userId,
        isVisited,
        isFavorite
    )
}