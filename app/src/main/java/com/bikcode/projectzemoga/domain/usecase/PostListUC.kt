package com.bikcode.projectzemoga.domain.usecase

import com.bikcode.projectzemoga.domain.model.PostDAO
import com.bikcode.projectzemoga.domain.repository.PostRepository
import kotlinx.coroutines.flow.Flow

class PostListUC(private val postRepository: PostRepository) {
    operator fun invoke(): Flow<List<PostDAO>> = postRepository.getPostsLocal()
}