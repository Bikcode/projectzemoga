package com.bikcode.projectzemoga.domain.usecase

import com.bikcode.projectzemoga.domain.repository.PostRepository

class SavePostUC(private val postRepository: PostRepository) {
    suspend operator fun invoke() = postRepository.savePost()
}