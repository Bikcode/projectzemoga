package com.bikcode.projectzemoga.domain.model

import com.bikcode.projectzemoga.presentation.model.Comment

data class CommentDAO(
    val body: String,
    val email: String,
    val id: Int,
    val name: String,
    val postId: Int,
) {
    fun commentToPresentation(): Comment = Comment(
        body,
        email,
        id,
        name,
        postId
    )
}