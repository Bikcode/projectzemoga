package com.bikcode.projectzemoga.domain.usecase

import com.bikcode.projectzemoga.domain.model.CommentDAO
import com.bikcode.projectzemoga.domain.repository.CommentRepository
import kotlinx.coroutines.flow.Flow

class CommentUC(private val commentRepository: CommentRepository) {
    operator fun invoke(postId: Int): Flow<List<CommentDAO>?> {
        try {
            return commentRepository.getCommentsFromPost(postId)
        } catch (exception: Exception) {
            throw exception
        }
    }
}