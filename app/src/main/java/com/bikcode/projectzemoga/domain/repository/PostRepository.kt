package com.bikcode.projectzemoga.domain.repository

import com.bikcode.projectzemoga.domain.model.PostDAO
import kotlinx.coroutines.flow.Flow

interface PostRepository {
    fun getPostsLocal(): Flow<List<PostDAO>>
    fun getFavoritePosts(): Flow<List<PostDAO>>
    suspend fun savePost()
    suspend fun updatePost(postDAO: PostDAO)
    suspend fun deletePosts()
    suspend fun deleteIndividualPost(postDAO: PostDAO): Int
}