package com.bikcode.projectzemoga.domain.usecase

import com.bikcode.projectzemoga.domain.model.PostDAO
import com.bikcode.projectzemoga.domain.repository.PostRepository

class UpdatePostUC(private val postRepository: PostRepository) {
    suspend operator fun invoke(post: PostDAO) {
        postRepository.updatePost(post)
    }
}