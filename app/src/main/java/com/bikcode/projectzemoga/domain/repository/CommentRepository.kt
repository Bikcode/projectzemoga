package com.bikcode.projectzemoga.domain.repository

import com.bikcode.projectzemoga.domain.model.CommentDAO
import kotlinx.coroutines.flow.Flow

interface CommentRepository {
    fun getCommentsFromPost(postId: Int): Flow<List<CommentDAO>?>
}