package com.bikcode.projectzemoga.domain.repository

import com.bikcode.projectzemoga.domain.model.UserDAO

interface UserRepository {
    suspend fun getUserFromPost(userId: Int): UserDAO?
}