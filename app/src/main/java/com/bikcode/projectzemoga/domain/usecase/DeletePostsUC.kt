package com.bikcode.projectzemoga.domain.usecase

import com.bikcode.projectzemoga.domain.repository.PostRepository

class DeletePostsUC(private val postRepository: PostRepository) {
    suspend operator fun invoke() {
        postRepository.deletePosts()
    }
}