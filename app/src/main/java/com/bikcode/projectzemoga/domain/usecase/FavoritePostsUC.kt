package com.bikcode.projectzemoga.domain.usecase

import com.bikcode.projectzemoga.domain.repository.PostRepository

class FavoritePostsUC(private val postRepository: PostRepository) {
    operator fun invoke() = postRepository.getFavoritePosts()
}