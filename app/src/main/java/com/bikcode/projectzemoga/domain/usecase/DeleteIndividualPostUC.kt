package com.bikcode.projectzemoga.domain.usecase

import com.bikcode.projectzemoga.domain.model.PostDAO
import com.bikcode.projectzemoga.domain.repository.PostRepository

class DeleteIndividualPostUC(private val postRepository: PostRepository) {
    suspend operator fun invoke(post: PostDAO) = postRepository.deleteIndividualPost(post)
}