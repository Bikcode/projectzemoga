package com.bikcode.projectzemoga.presentation.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import com.bikcode.projectzemoga.R
import com.bikcode.projectzemoga.databinding.ActivityPostDetailBinding
import com.bikcode.projectzemoga.presentation.adapter.CommentAdapter
import com.bikcode.projectzemoga.presentation.extension.gone
import com.bikcode.projectzemoga.presentation.extension.snack
import com.bikcode.projectzemoga.presentation.model.Post
import com.bikcode.projectzemoga.presentation.model.User
import com.bikcode.projectzemoga.presentation.state.DetailState
import com.bikcode.projectzemoga.presentation.viewmodel.MainViewModel
import com.bikcode.projectzemoga.presentation.viewmodel.PostDetailViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PostDetailActivity : AppCompatActivity() {

    private lateinit var _binding: ActivityPostDetailBinding
    private var _post: Post? = null
    private val post get() = _post!!
    private val postDetailViewModel: PostDetailViewModel by viewModels<PostDetailViewModel>()
    private val mainViewModel: MainViewModel by viewModels<MainViewModel>()
    private val _commentAdapter: CommentAdapter by lazy { CommentAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityPostDetailBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        init()
        setupObserver()
        setupListener()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun init() {
        intent?.let {
            _post = it.getParcelableExtra<Post>(EXTRA_POST)
            setPostData(post)
        }
        setSupportActionBar(_binding.tbDetail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun setupListener() {
        with(_binding) {
            ibFavorite.setOnClickListener {
                val isFavoritePost = post.isFavorite.not()
                mainViewModel.update(post.copy(isFavorite = isFavoritePost))
                toggleIsFavorite(isFavoritePost)
            }
        }
    }

    private fun toggleIsFavorite(isFavorite: Boolean) {
        val iconStar = if (isFavorite) {
            R.drawable.ic_baseline_star
        } else {
            R.drawable.ic_baseline_star_border
        }
        _binding.ibFavorite.setImageResource(iconStar)
    }

    private fun setPostData(post: Post?) {
        post?.let {
            setupRecycler()
            with(_binding) {
                tvTitleDetailDescription.text = post.body
            }
            postDetailViewModel.getComments(post.id)
            postDetailViewModel.getUser(post.userId)
            toggleIsFavorite(post.isFavorite)
        } ?: finish()
    }

    private fun setupRecycler() {
        _binding.rvComments.apply {
            adapter = _commentAdapter
            addItemDecoration(DividerItemDecoration(this@PostDetailActivity,
                DividerItemDecoration.VERTICAL))
        }
    }

    private fun setUserData(user: User) {
        with(_binding) {
            tvName.text = user.name
            tvEmail.text = user.email
            tvPhone.text = user.phone
            tvWebsite.text = user.website
            pbLoadingUser.gone()
        }
    }

    private fun setupObserver() {
        postDetailViewModel.detailState.observe(this, { state ->
            when (state) {
                is DetailState.OnErrorUser -> {
                    _binding.root.snack(getString(R.string.internal_error))
                    _binding.pbLoadingUser.gone()
                }
                is DetailState.OnErrorComments -> {
                    _binding.root.snack(getString(R.string.internal_error))
                    _binding.pbLoadingComments.gone()
                }
                is DetailState.OnSuccessComments -> {
                    _commentAdapter.setData(state.comments)
                    _binding.pbLoadingComments.gone()
                }
                is DetailState.OnSuccessUser -> {
                    setUserData(state.user)
                }
                DetailState.CommentsNull -> {
                    _binding.rvComments.gone()
                    _binding.pbLoadingComments.gone()
                }
                DetailState.UserNull -> {
                    _binding.groupUserInfo.gone()
                    _binding.pbLoadingUser.gone()
                }
            }
        })
    }

    companion object {
        private const val EXTRA_POST = "post"
        fun launch(context: Context, post: Post) {
            val intent = Intent(context, PostDetailActivity::class.java).apply {
                putExtra(EXTRA_POST, post)
            }
            context.startActivity(intent)
        }
    }
}