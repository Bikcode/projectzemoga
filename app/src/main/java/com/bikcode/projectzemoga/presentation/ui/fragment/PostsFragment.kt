package com.bikcode.projectzemoga.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.bikcode.projectzemoga.R
import com.bikcode.projectzemoga.databinding.FragmentPostsBinding
import com.bikcode.projectzemoga.presentation.adapter.PostAdapter
import com.bikcode.projectzemoga.presentation.adapter.SwipeToDeleteCallback
import com.bikcode.projectzemoga.presentation.extension.gone
import com.bikcode.projectzemoga.presentation.extension.snack
import com.bikcode.projectzemoga.presentation.extension.visible
import com.bikcode.projectzemoga.presentation.state.PostState
import com.bikcode.projectzemoga.presentation.ui.activity.PostDetailActivity
import com.bikcode.projectzemoga.presentation.util.isNetworkAvailable
import com.bikcode.projectzemoga.presentation.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PostsFragment : Fragment() {

    private var _binding: FragmentPostsBinding? = null
    private val binding get() = _binding!!
    private val mainViewModel by viewModels<MainViewModel>()
    private lateinit var postAdapter: PostAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentPostsBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapter()
        setupObserver()
        setupListeners()
        validateNetworkAndFetchData()
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }

    private fun setupAdapter() {
        postAdapter = PostAdapter { post ->
            mainViewModel.update(post.copy(isVisited = true))
        }
        binding.rvPosts.apply {
            adapter = postAdapter
            addItemDecoration(DividerItemDecoration(requireContext(),
                DividerItemDecoration.VERTICAL))
        }

        val swipeHandler = object : SwipeToDeleteCallback(requireContext()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val post = postAdapter.currentList[viewHolder.absoluteAdapterPosition]
                mainViewModel.deleteIndividualPost(post)
                postAdapter.notifyItemRemoved(viewHolder.absoluteAdapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(binding.rvPosts)
    }

    private fun setupListeners() {
        binding.fbDeleteAll.setOnClickListener {
            mainViewModel.deletePosts()
        }

        binding.layoutNoConnectionPosts.fabRetry.setOnClickListener {
            if(isNetworkAvailable(requireContext())) {
                mainViewModel.fetchAndSavePosts()
            }
        }
    }

    private fun setupObserver() {
        mainViewModel.postState.observe(viewLifecycleOwner, { postState ->
            when (postState) {
                is PostState.OnError -> {
                    println(postState.message)
                    binding.pbLoading.gone()
                    binding.groupContainer.gone()
                }
                PostState.HideLoading -> binding.pbLoading.gone()
                PostState.ShowLoading -> binding.pbLoading.visible()
                PostState.EmptyList -> {
                    postAdapter.submitList(emptyList())
                    binding.groupContainer.gone()
                    binding.groupEmptyPosts.visible()
                }
                PostState.DeletedPost -> view?.snack(getString(R.string.post_deleted))
                PostState.ErrorDeletePost -> view?.snack(getString(R.string.error_post_delete))
                is PostState.UpdatedPost -> {
                    PostDetailActivity.launch(requireContext(), postState.post)
                }
            }
        })

        mainViewModel.postsLivedata.observe(viewLifecycleOwner, { posts ->
            if (posts.isNotEmpty()) {
                postAdapter.submitList(posts)
                binding.pbLoading.gone()
                binding.groupContainer.visible()
                binding.groupEmptyPosts.gone()
                if(binding.layoutNoConnectionPosts.root.isVisible){
                    binding.layoutNoConnectionPosts.root.gone()
                }
            } else {
                if(isNetworkAvailable(requireContext())){
                    binding.groupEmptyPosts.visible()
                }
            }
        })
    }

    private fun validateNetworkAndFetchData() {
        if(isNetworkAvailable(requireContext())) {
            mainViewModel.fetchAndSavePosts()
        } else {
            binding.layoutNoConnectionPosts.root.visible()
            binding.pbLoading.gone()
        }
    }

    companion object {
        fun newInstance(bundle: Bundle? = null): Fragment = PostsFragment().apply {
            arguments = bundle
        }
    }
}