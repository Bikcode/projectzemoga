package com.bikcode.projectzemoga.presentation.state

import com.bikcode.projectzemoga.presentation.model.Comment
import com.bikcode.projectzemoga.presentation.model.User

sealed class DetailState {
    object UserNull: DetailState()
    object CommentsNull: DetailState()
    data class OnSuccessUser(val user: User): DetailState()
    data class OnSuccessComments(val comments: List<Comment>): DetailState()
    data class OnErrorUser(val message: String?): DetailState()
    data class OnErrorComments(val message: String?): DetailState()
}