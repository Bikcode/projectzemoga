package com.bikcode.projectzemoga.presentation.state

import com.bikcode.projectzemoga.presentation.model.Post

sealed class PostState {
    object ShowLoading : PostState()
    object HideLoading : PostState()
    object EmptyList : PostState()
    object DeletedPost: PostState()
    data class UpdatedPost(val post: Post): PostState()
    object ErrorDeletePost: PostState()
    data class OnError(val message: String?) : PostState()
}