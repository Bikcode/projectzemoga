package com.bikcode.projectzemoga.presentation.ui.activity

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.bikcode.projectzemoga.R
import com.bikcode.projectzemoga.databinding.ActivityMainBinding
import com.bikcode.projectzemoga.presentation.adapter.FragmentAdapter
import com.bikcode.projectzemoga.presentation.util.isNetworkAvailable
import com.bikcode.projectzemoga.presentation.viewmodel.MainViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var _binding: ActivityMainBinding
    private lateinit var fragmentAdapter: FragmentAdapter
    private val mainViewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        setupListeners()
        setupViewPager()
    }

    private fun setupViewPager() {
        fragmentAdapter = FragmentAdapter(
            supportFragmentManager,
            lifecycle
        )
        with(_binding) {
            vpContainer.adapter = fragmentAdapter
            tlPages.addTab(tlPages.newTab().setText(getString(R.string.main_first_tab_description)))
            tlPages.addTab(tlPages.newTab()
                .setText(getString(R.string.main_second_tab_description)))

            tlPages.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    vpContainer.currentItem = tab.position
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {}

                override fun onTabReselected(tab: TabLayout.Tab?) {}
            })
            vpContainer.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    tlPages.selectTab(tlPages.getTabAt(position))
                }
            })
        }
    }

    private fun setupListeners() {
        _binding.ibRefresh.setOnClickListener {
            val isConnected = isNetworkAvailable(this@MainActivity)
            if (isConnected) {
                mainViewModel.fetchAndSavePosts()
            } else {
                Snackbar.make(_binding.root,
                    getString(R.string.no_connection),
                    Snackbar.LENGTH_SHORT).show()
            }
        }
    }
}