package com.bikcode.projectzemoga.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import com.bikcode.projectzemoga.R
import com.bikcode.projectzemoga.databinding.FragmentFavoritePostsBinding
import com.bikcode.projectzemoga.presentation.adapter.PostAdapter
import com.bikcode.projectzemoga.presentation.extension.gone
import com.bikcode.projectzemoga.presentation.extension.snack
import com.bikcode.projectzemoga.presentation.extension.visible
import com.bikcode.projectzemoga.presentation.state.FavoriteState
import com.bikcode.projectzemoga.presentation.viewmodel.FavoriteViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FavoritePostsFragment : Fragment() {

    private var _binding: FragmentFavoritePostsBinding? = null
    private val binding get() = _binding!!

    private val favoriteViewModel by viewModels<FavoriteViewModel>()
    private lateinit var postAdapter: PostAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentFavoritePostsBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObserver()
        setupAdapter()
        favoriteViewModel.getFavoritePosts()
    }

    private fun setupAdapter() {
        postAdapter = PostAdapter {}
        binding.rvFavoritePosts.apply {
            adapter = postAdapter
            addItemDecoration(DividerItemDecoration(requireContext(),
                DividerItemDecoration.VERTICAL))
        }
    }

    private fun setupObserver() {
        favoriteViewModel.favoriteState.observe(viewLifecycleOwner, { state ->
            when (state) {
                FavoriteState.EmptyList -> {
                    hideViews()
                }
                is FavoriteState.OnSuccessFavoriteList -> {
                    postAdapter.submitList(state.data)
                    binding.rvFavoritePosts.visible()
                    binding.groupEmptyFavoritePosts.gone()
                }
                is FavoriteState.OnError -> {
                    hideViews()
                    view?.snack(state.message ?: getString(R.string.internal_error))
                }
            }
        })
    }

    private fun hideViews() {
        binding.groupEmptyFavoritePosts.visible()
        postAdapter.submitList(emptyList())
        binding.rvFavoritePosts.gone()
    }

    companion object {
        fun newInstance(bundle: Bundle? = null): Fragment = FavoritePostsFragment().apply {
            arguments = bundle
        }
    }
}