package com.bikcode.projectzemoga.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bikcode.projectzemoga.databinding.ItemCommentBinding
import com.bikcode.projectzemoga.databinding.ItemCommentHeaderBinding
import com.bikcode.projectzemoga.presentation.model.Comment

class CommentAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val _comments = mutableListOf<Comment>()

    fun setData(comments: List<Comment>) {
        _comments.clear()
        _comments.addAll(comments)
        notifyDataSetChanged()
    }

    inner class CommentContentViewHolder(private val binding: ItemCommentBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(comment: Comment) {
            binding.tvComment.text = comment.body
        }
    }

    inner class CommentHeaderViewHolder(binding: ItemCommentHeaderBinding) :
        RecyclerView.ViewHolder(binding.root) {}

    override fun getItemViewType(position: Int): Int {
        return if (isHeader(position)) HEADER_POSITION else CONTENT_POSITION
    }

    private fun isHeader(position: Int) = position == HEADER_POSITION

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == HEADER_POSITION) {
            val binding = ItemCommentHeaderBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            CommentHeaderViewHolder(binding)
        } else {
            val binding = ItemCommentBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            CommentContentViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CommentContentViewHolder) {
            holder.bind(_comments[position])
        }
    }

    override fun getItemCount(): Int = _comments.count()
}

private const val HEADER_POSITION = 0
private const val CONTENT_POSITION = 1