package com.bikcode.projectzemoga.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bikcode.projectzemoga.domain.usecase.FavoritePostsUC
import com.bikcode.projectzemoga.presentation.state.FavoriteState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(
    private val favoritePostsUC: FavoritePostsUC,
    private val _favoriteState: MutableLiveData<FavoriteState>,
) : ViewModel() {
    val favoriteState: LiveData<FavoriteState> get() = _favoriteState

    fun getFavoritePosts() {
        viewModelScope.launch {
            try {
                favoritePostsUC.invoke().collect {
                    if (it.count() > 0) {
                        _favoriteState.value =
                            FavoriteState.OnSuccessFavoriteList(it.map { postDAO ->
                                postDAO.postToPresentation()
                            })
                    } else {
                        _favoriteState.value = FavoriteState.EmptyList
                    }
                }
            } catch (exception: Exception) {
                _favoriteState.value = FavoriteState.OnError(exception.message)
            }
        }
    }
}