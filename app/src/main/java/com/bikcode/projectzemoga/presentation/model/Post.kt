package com.bikcode.projectzemoga.presentation.model

import android.os.Parcelable
import com.bikcode.projectzemoga.domain.model.PostDAO
import kotlinx.parcelize.Parcelize

@Parcelize
data class Post(
    val body: String,
    val id: Int,
    val title: String,
    val userId: Int,
    val isVisited: Boolean,
    val isFavorite: Boolean,
) : Parcelable {
    fun postToDomain(): PostDAO = PostDAO(
        id,
        body,
        title,
        userId,
        isVisited,
        isFavorite
    )
}