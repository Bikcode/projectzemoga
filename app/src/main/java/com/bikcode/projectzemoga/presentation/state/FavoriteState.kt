package com.bikcode.projectzemoga.presentation.state

import com.bikcode.projectzemoga.presentation.model.Post

sealed class FavoriteState {
    object EmptyList : FavoriteState()
    data class OnError(val message: String? = ""): FavoriteState()
    data class OnSuccessFavoriteList(val data: List<Post>): FavoriteState()
}