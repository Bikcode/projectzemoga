package com.bikcode.projectzemoga.presentation.model

data class Comment(
    val body: String,
    val email: String,
    val id: Int,
    val name: String,
    val postId: Int
) {

}