package com.bikcode.projectzemoga.presentation.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.bikcode.projectzemoga.presentation.ui.fragment.FavoritePostsFragment
import com.bikcode.projectzemoga.presentation.ui.fragment.PostsFragment

class FragmentAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount(): Int = TOTAL_PAGES

    override fun createFragment(position: Int): Fragment {
        return when(position) {
            POSTS_FRAGMENT_POSITION -> PostsFragment.newInstance()
            FAVORITE_POSTS_FRAGMENT_POSITION -> FavoritePostsFragment.newInstance()
            else -> PostsFragment.newInstance()
        }
    }
}

private const val TOTAL_PAGES = 2
private const val POSTS_FRAGMENT_POSITION = 0
private const val FAVORITE_POSTS_FRAGMENT_POSITION = 1