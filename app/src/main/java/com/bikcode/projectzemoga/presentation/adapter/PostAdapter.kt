package com.bikcode.projectzemoga.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bikcode.projectzemoga.databinding.ItemPostBinding
import com.bikcode.projectzemoga.presentation.extension.gone
import com.bikcode.projectzemoga.presentation.extension.visible
import com.bikcode.projectzemoga.presentation.model.Post

class PostAdapter(private val closure: (Post) -> Unit) :
    ListAdapter<Post, PostAdapter.PostViewHolder>(PostsDiffCallBack()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val binding = ItemPostBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return PostViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class PostViewHolder(private val binding: ItemPostBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(post: Post) {
            with(binding) {
                if (post.isVisited) {
                    ivReadIndicator.gone()
                } else {
                    ivReadIndicator.visible()
                }

                if (post.isFavorite) {
                    ivFavoriteIndicator.visible()
                } else {
                    ivFavoriteIndicator.gone()
                }

                tvDescription.text = post.title
            }
            binding.root.setOnClickListener {
                closure.invoke(post)
            }
        }
    }
}

private class PostsDiffCallBack : DiffUtil.ItemCallback<Post>() {
    override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
        return oldItem == newItem
    }
}