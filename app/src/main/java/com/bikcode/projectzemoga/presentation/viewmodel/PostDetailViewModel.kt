package com.bikcode.projectzemoga.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bikcode.projectzemoga.domain.usecase.CommentUC
import com.bikcode.projectzemoga.domain.usecase.UserUC
import com.bikcode.projectzemoga.presentation.state.DetailState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class PostDetailViewModel @Inject constructor(
    private val userUC: UserUC,
    private val commentUC: CommentUC,
    private val dispatcher: CoroutineDispatcher,
    private val _detailState: MutableLiveData<DetailState>,
) : ViewModel() {

    val detailState: LiveData<DetailState> get() = _detailState

    fun getUser(userId: Int) {
        viewModelScope.launch {
            try {
                val user = withContext(dispatcher) {
                    userUC.invoke(userId)?.userToPresentation()
                }
                user?.let {
                    _detailState.value = DetailState.OnSuccessUser(user)
                } ?: run {
                    _detailState.value = DetailState.UserNull
                }
            } catch (exception: Exception) {
                _detailState.value = DetailState.OnErrorUser(exception.message)
            }
        }
    }

    fun getComments(postId: Int) {
        viewModelScope.launch {
            try {
                commentUC.invoke(postId).catch {
                    _detailState.value = DetailState.OnErrorComments(it.message)
                }.collect { comments ->
                    comments?.let {
                        _detailState.value = DetailState.OnSuccessComments(it.map { commentDAO ->
                            commentDAO.commentToPresentation()
                        })
                    } ?: run {
                        _detailState.value = DetailState.CommentsNull
                    }
                }
            } catch (exception: Exception) {
                _detailState.value = DetailState.OnErrorComments(exception.message)
            }
        }
    }
}