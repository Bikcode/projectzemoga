package com.bikcode.projectzemoga.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.bikcode.projectzemoga.domain.usecase.DeleteIndividualPostUC
import com.bikcode.projectzemoga.domain.usecase.DeletePostsUC
import com.bikcode.projectzemoga.domain.usecase.PostListUC
import com.bikcode.projectzemoga.domain.usecase.SavePostUC
import com.bikcode.projectzemoga.domain.usecase.UpdatePostUC
import com.bikcode.projectzemoga.presentation.model.Post
import com.bikcode.projectzemoga.presentation.state.PostState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val savePostUC: SavePostUC,
    postListUC: PostListUC,
    private val updatePostUC: UpdatePostUC,
    private val deletePostsUC: DeletePostsUC,
    private val deleteIndividualPostUC: DeleteIndividualPostUC,
    private val dispatcher: CoroutineDispatcher,
    private val _postsState: MutableLiveData<PostState>,
) : ViewModel() {

    val postState: LiveData<PostState> get() = _postsState
    val postsLivedata: LiveData<List<Post>> = postListUC.invoke().map {
        it.map { postDAO ->
            postDAO.postToPresentation()
        }
    }.asLiveData()

    fun fetchAndSavePosts() {
        viewModelScope.launch {
            withContext(dispatcher) {
                savePostUC.invoke()
            }
        }
    }

    fun update(post: Post) {
        viewModelScope.launch {
            withContext(dispatcher) {
                updatePostUC.invoke(post.postToDomain())
            }
            _postsState.value = PostState.UpdatedPost(post)
        }
    }

    fun deletePosts() {
        viewModelScope.launch {
            withContext(dispatcher) {
                deletePostsUC.invoke()
            }
            _postsState.value = PostState.EmptyList
        }
    }

    fun deleteIndividualPost(post: Post) {
        viewModelScope.launch {
            val deleted = withContext(dispatcher) {
                deleteIndividualPostUC.invoke(post.postToDomain())
            }
            if (deleted > 0) {
                _postsState.value = PostState.DeletedPost
            } else {
                _postsState.value = PostState.ErrorDeletePost
            }
        }
    }
}