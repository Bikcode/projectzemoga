package com.bikcode.projectzemoga.data.remote.services

import com.bikcode.projectzemoga.data.remote.Endpoints
import com.bikcode.projectzemoga.data.remote.dto.PostDTO
import retrofit2.Response
import retrofit2.http.GET

interface PostService {

    @GET(Endpoints.posts)
    suspend fun getAllPosts(): Response<List<PostDTO>>
}