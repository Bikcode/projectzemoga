package com.bikcode.projectzemoga.data.remote.dto

import com.bikcode.projectzemoga.domain.model.PostDAO
import com.squareup.moshi.Json

data class PostDTO(
    @Json(name = "body") val body: String,
    @Json(name = "id") val id: Int,
    @Json(name = "title") val title: String,
    @Json(name = "userId") val userId: Int,
) {
    fun postToDomain(isVisited: Boolean) = PostDAO(
        body = body,
        id = id,
        title = title,
        userId = userId,
        isVisited = isVisited,
        isFavorite = false
    )
}