package com.bikcode.projectzemoga.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.bikcode.projectzemoga.data.local.dao.PostDao
import com.bikcode.projectzemoga.domain.entity.PostEntity

@Database(entities = [PostEntity::class], version = 1)
abstract class AppDatabase: RoomDatabase() {

    abstract fun postDao(): PostDao

    companion object {
        const val DATABASE_NAME = "postdatabase"
    }
}