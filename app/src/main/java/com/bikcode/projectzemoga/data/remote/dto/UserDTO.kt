package com.bikcode.projectzemoga.data.remote.dto

import com.bikcode.projectzemoga.domain.model.UserDAO
import com.squareup.moshi.Json

data class UserDTO(
    @Json(name = "id") val id: Int,
    @Json(name = "name") val name: String,
    @Json(name = "email") val email: String,
    @Json(name = "phone") val phone: String,
    @Json(name = "website") val website: String,
) {
    fun userToDAO(): UserDAO = UserDAO(
        id,
        name,
        email,
        phone,
        website
    )
}