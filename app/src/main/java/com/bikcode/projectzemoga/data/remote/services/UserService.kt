package com.bikcode.projectzemoga.data.remote.services

import com.bikcode.projectzemoga.data.remote.Endpoints
import com.bikcode.projectzemoga.data.remote.dto.UserDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface UserService {

    @GET("${Endpoints.users}/{userId}")
    suspend fun getUser(
        @Path("userId") userId: String
    ): Response<UserDTO>
}