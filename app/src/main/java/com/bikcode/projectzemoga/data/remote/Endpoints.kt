package com.bikcode.projectzemoga.data.remote

object Endpoints {
    /** endpoints */
    const val posts = "posts"
    const val users = "users"
    const val comments = "comments"
}