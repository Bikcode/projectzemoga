package com.bikcode.projectzemoga.data.repository

import com.bikcode.projectzemoga.data.remote.services.UserService
import com.bikcode.projectzemoga.domain.model.UserDAO
import com.bikcode.projectzemoga.domain.repository.UserRepository

class UserRepositoryImpl(private val userService: UserService): UserRepository {
    override suspend fun getUserFromPost(userId: Int): UserDAO? {
        try {
            val response = userService.getUser(userId.toString())
            return if(response.isSuccessful) {
                response.body()?.userToDAO()
            } else null
        }catch (exception: Exception) {
            throw exception
        }
    }
}