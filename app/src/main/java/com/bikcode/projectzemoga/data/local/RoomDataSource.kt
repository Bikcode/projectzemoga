package com.bikcode.projectzemoga.data.local

import com.bikcode.projectzemoga.data.local.dao.PostDao
import com.bikcode.projectzemoga.domain.datasource.LocalDataSource
import com.bikcode.projectzemoga.domain.entity.PostEntity
import com.bikcode.projectzemoga.domain.model.PostDAO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class RoomDataSource(private val postDao: PostDao) : LocalDataSource {

    override suspend fun savePost(postEntity: PostEntity) {
        postDao.savePost(postEntity)
    }

    override fun getPosts(): Flow<List<PostDAO>> = postDao.getPosts().map {
        it.map { postEntity ->
            postEntity.postToDAO()
        }
    }

    override suspend fun getCount(): Int {
        return postDao.count()
    }

    override suspend fun updatePost(postDAO: PostDAO) {
        postDao.updatePost(postDAO.postToEntity())
    }

    override suspend fun deletePosts() {
        postDao.clearData()
    }

    override fun getFavoritePosts(): Flow<List<PostDAO>> = postDao.getFavoritePosts().map {
        it.map { postEntity ->
            postEntity.postToDAO()
        }
    }

    override suspend fun deletePost(post: PostDAO): Int {
        return postDao.deletePost(post.postToEntity())
    }
}