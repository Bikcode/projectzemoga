package com.bikcode.projectzemoga.data.remote.services

import com.bikcode.projectzemoga.data.remote.Endpoints
import com.bikcode.projectzemoga.data.remote.dto.CommentDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CommentService {

    @GET(Endpoints.comments)
    suspend fun getAllComments(
        @Query("postId") postId: String,
    ): Response<List<CommentDTO>>
}