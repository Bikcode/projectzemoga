package com.bikcode.projectzemoga.data.repository

import com.bikcode.projectzemoga.data.remote.services.PostService
import com.bikcode.projectzemoga.domain.datasource.LocalDataSource
import com.bikcode.projectzemoga.domain.model.PostDAO
import com.bikcode.projectzemoga.domain.repository.PostRepository
import kotlinx.coroutines.flow.Flow

class PostRepositoryImpl(
    private val postService: PostService,
    private val postUnread: Int,
    private val localDataSource: LocalDataSource,
) : PostRepository {

    override suspend fun savePost() {
        if (localDataSource.getCount() == 0) {
            try {
                val data = postService.getAllPosts().body()?.mapIndexed { index, postDTO ->
                    if (index < postUnread) {
                        postDTO.postToDomain(isVisited = false)
                    } else {
                        postDTO.postToDomain(isVisited = true)
                    }
                }
                if (data?.isNotEmpty() == true) {
                    savePostsLocal(data)
                }
            } catch (exception: Exception) {
                throw exception
            }
        }
    }

    override fun getPostsLocal(): Flow<List<PostDAO>> = localDataSource.getPosts()

    override fun getFavoritePosts(): Flow<List<PostDAO>> = localDataSource.getFavoritePosts()

    override suspend fun updatePost(postDAO: PostDAO) {
        localDataSource.updatePost(postDAO)
    }

    override suspend fun deletePosts() {
        localDataSource.deletePosts()
    }

    override suspend fun deleteIndividualPost(postDAO: PostDAO): Int {
        return localDataSource.deletePost(postDAO)
    }

    private suspend fun savePostsLocal(posts: List<PostDAO>) {
        if (posts.isNotEmpty()) {
            posts.forEach { postDAO ->
                localDataSource.savePost(postDAO.postToEntity())
            }
        }
    }
}