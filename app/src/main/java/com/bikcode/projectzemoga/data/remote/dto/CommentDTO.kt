package com.bikcode.projectzemoga.data.remote.dto

import com.bikcode.projectzemoga.domain.model.CommentDAO
import com.squareup.moshi.Json

data class CommentDTO(
    @Json(name = "body") val body: String,
    @Json(name = "email") val email: String,
    @Json(name = "id") val id: Int,
    @Json(name = "name") val name: String,
    @Json(name = "postId") val postId: Int,
) {
    fun commentToDAO(): CommentDAO = CommentDAO(
        body,
        email,
        id,
        name,
        postId
    )
}