package com.bikcode.projectzemoga.data.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.bikcode.projectzemoga.domain.entity.PostEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface PostDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun savePost(postEntity: PostEntity)

    @Query("SELECT * FROM post")
    fun getPosts(): Flow<List<PostEntity>>

    @Query("SELECT * FROM post WHERE isFavorite = 1")
    fun getFavoritePosts(): Flow<List<PostEntity>>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updatePost(postEntity: PostEntity)

    @Query("DELETE FROM post")
    suspend fun clearData()

    @Query("SELECT COUNT(id) FROM post")
    suspend fun count(): Int

    @Delete
    suspend fun deletePost(post: PostEntity): Int
}