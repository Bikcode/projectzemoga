package com.bikcode.projectzemoga.data.repository

import com.bikcode.projectzemoga.data.remote.services.CommentService
import com.bikcode.projectzemoga.domain.model.CommentDAO
import com.bikcode.projectzemoga.domain.repository.CommentRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class CommentRepositoryImpl(private val commentService: CommentService) : CommentRepository {
    override fun getCommentsFromPost(postId: Int): Flow<List<CommentDAO>?> = flow {
        try {
            val response = commentService.getAllComments(postId.toString())
            if (response.isSuccessful) {
                val data = response.body()?.map {
                    it.commentToDAO()
                }
                emit(data)
            } else {
                emit(null)
            }
        } catch (exception: Exception) {
            throw exception
        }
    }
}