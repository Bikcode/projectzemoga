# Posts App

Native android app build in kotlin as a main language in which you can see a list of post from jsonplace holder

## What you will see in this project

You can also swipe to delete each post or touch each item to go and see post detail with user author and comments related to the post, in the detail you can select the post as favorite as well.

The first 20 posts has a blue indicator and when the post is readed the blue indicator is gone

* Kotlin (programming language recommended for Google for native android development
* Room (dependency for local persistence with SQLite)
* Retrofit (dependency for make http request to https://jsonplaceholder.typicode.co/)
* Dagger Hilt (Dependency injection for repositories, use cases, livedata and room)
* Coroutines y LiveData for handle asynchronous data and process
* Lottie animation  for animations images for a better user experience
* Mockito for unit testing


# Description of each screen:

### Posts list screen

This is the initial screen, when the app starts then find posts stored in room, if is empty then the remote service is invocated and fetch data and it's saved locally.

The user can swipe to left to delete each post or can delete all post touching the float action button and fetch again the data from remote service touching the refresh buttom in right top corner.

If the device has not internet connection it will display a lottie animation indicating that device is not connected to internet when the user try to fetch data of the remote service.

### Favorite posts screen:

In this screen are all the favorite posts of the user

### Post detail screen: 

In this screen the user can see all the body of the post, theuser information who wrote the post and the comments of the post.

In the right top corner has a start button that can touch to toggle as a favorite or no favorite the post.

# Architecture

Esta aplicación está desarrollada en lenguaje Kotlin, siguiendo el patrón de arquitectura Model View View Model (MVVM), haciendo uso de patrón Repository, y siguiendo el enfoque de arquitectura limpia (Clean Architecture)

This app has been developed with kotlin using MVVM(Model View View Model) pattern and repository pattern too using a clean arquitecture.


# Responsability of each layer

Below is a technical description of each layer, raised in this project:

### Data layer:

This layer has the implementations of the repositories logic from domain layer, this repositories has the responsability of get and handle data from remote and locale services, this layer depends on domain layer.

### Domain layer:

This is the deepest layer, here you can see all the businness rules, core logic of the app, you could see all the interfaces of the repositories and localdatasource for local storage, you can see the dao model, entity model as well, this layer doesn't depend of others.


### Presentation layer:

This layer is the one that interacts with the user throught the different views like recyclerviews, textviews etc. Here you can see activities, fragments, viewmodels, adapters.
To handle state of the data is used livedata with coroutines.

### Screens

Posts
--- 
![](https://bitbucket.org/Bikcode/projectzemoga/raw/0775d8344b5e1b2a8a3ae039eda5815abf2235a4/app/src/main/assets/images/posts.jpeg)

Favorite posts
---
![](https://bitbucket.org/Bikcode/projectzemoga/raw/0775d8344b5e1b2a8a3ae039eda5815abf2235a4/app/src/main/assets/images/favorite_posts.jpeg)

Detail post
---
![](https://bitbucket.org/Bikcode/projectzemoga/raw/0775d8344b5e1b2a8a3ae039eda5815abf2235a4/app/src/main/assets/images/favorite_posts.jpeg)

Empty posts
---
![](https://bitbucket.org/Bikcode/projectzemoga/raw/0775d8344b5e1b2a8a3ae039eda5815abf2235a4/app/src/main/assets/images/empty_posts.jpeg)

Favorite posts
---
![](https://bitbucket.org/Bikcode/projectzemoga/raw/0775d8344b5e1b2a8a3ae039eda5815abf2235a4/app/src/main/assets/images/empty_favorite.jpeg)

No connection
---
![](https://bitbucket.org/Bikcode/projectzemoga/raw/0775d8344b5e1b2a8a3ae039eda5815abf2235a4/app/src/main/assets/images/no_connection.jpeg)

### How to run the app:
 For use this app, the api of the device has to be an API 23 or  Android 6.0 Marshmallow
- Import the project to android studio
- Wait until gradle finish all its process
- Has an internet connection
- Has an emulator running or use physical device with depuration USB enabled for install
- Has wifi enabled and connection
- In android studio click on play "Run" or you can press from keyboard Shift + F10 and enjoy